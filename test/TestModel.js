const { mongoose: { model, Schema } } = require('./DB');

const schema = new Schema({
  defaultString: { type: String, locale: true },
  extraConfig: { type: String, locale: { languages: ['de'] } },
  normalString: { type: String },
});

schema.plugin(require('../lib/mongoose-locale'), { languages: ['en', 'zh'], defaultLanguage: 'en' });

module.exports = model('Test', schema);
