const mongoose = require('mongoose');

module.exports = {
  mongoose,
  connect: mongoose.connect(
    process.env.MONGO_URL,
    {
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  ),
};
