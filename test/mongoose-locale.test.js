const { mongoose: { Schema, model }, connect } = require('./DB');
// const TestModel = require('./TestModel');
const locale = require('../lib/mongoose-locale');

describe('Initiate', () => {
  let connection;
  beforeAll(async () => {
    connection = await connect;
  });

  it('should throw error if config is missing', () => {
    const schema = new Schema();
    expect(() => { schema.plugin(locale); }).toThrowError();
  });

  it('should throw error if language array is empty', () => {
    const schema = new Schema();
    expect(() => { schema.plugin(locale, { languages: [] }); }).toThrowError();
  });

  it('should throw error if default language is not in language config', () => {
    const schema = new Schema();
    expect(() => { schema.plugin(locale, { languages: ['en'], defaultLanguage: 'de' }); }).toThrowError();
  });

  it('should throw error if type is not string', () => {
    const schema = new Schema({ wrongType: { type: Number, locale: true } });
    expect(() => { schema.plugin(locale, { languages: ['en'] }); }).toThrowError();
  });

  afterAll(async () => {
    connection.close();
  });
});

describe('Creation', () => {
  let connection;
  beforeAll(async () => {
    connection = await connect;
  });

  it('should save a map object in the locale field and getter in the original field', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test', schema);
    const test = new Test({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    await test.save();
    expect(test.hello_locale).toBeDefined();
    expect(test.hello).toBeDefined();
  });

  it('should save a map object in a specified locale postfix', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'], localePostfix: 'Dictionary' });
    const Test = model('Test2', schema);
    const test = new Test({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    await test.save();
    expect(test.helloDictionary).toBeDefined();
    expect(test.hello).toBeDefined();
  });

  afterAll(async () => {
    connection.close();
  });
});

describe('Query', () => {
  let connection;
  beforeAll(async () => {
    connection = await connect;
  });

  it('should able to handle new query option', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test3', schema);
    const test = new Test({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    await test.save();

    const record = await Test.findById(test.id).language('de').exec();
    expect(record.hello).toMatch('Hallo Welt');
  });

  it('should handle multiple record query', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test6', schema);
    await Promise.all([
      Test.create({
        hello: {
          en: 'Hello World',
          de: 'Hallo Welt',
        },
      }),
      Test.create({
        hello: {
          en: 'Hello World',
          de: 'Hallo Welt',
        },
      }),
      Test.create({
        hello: {
          en: 'Hello World',
          de: 'Hallo Welt',
        },
      }),
    ]);
    const records = await Test.find().language('de').exec();
    records.forEach((record) => {
      expect(record.hello).toMatch('Hallo Welt');
    });
  });

  it.only('should update record by query', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test7', schema);
    const { id } = await Test.create({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    const record = await Test.findByIdAndUpdate(id, { hello_locale: { de: 'Hallo Neue Welt' } }, { new: true }).exec();
    console.log({ record });
    expect(record.hello).toMatch('Hallo Welt');
  });

  afterAll(async () => {
    connection.close();
  });
});

describe('Usage', () => {
  let connection;
  beforeAll(async () => {
    connection = await connect;
  });

  it('should return the value in default language in document', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test', schema);
    const test = new Test({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    await test.save();
    expect(test.hello).toMatch('Hello World');
  });

  it('should able to change active language in document', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test2', schema);
    const test = new Test({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    await test.save();
    test.setLocale('de');
    expect(test.hello).toMatch('Hallo Welt');
  });

  it('should update default language in document', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test4', schema);
    const test = new Test({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    await test.save();
    test.hello = 'Hello NEW World';
    expect(test.hello).toMatch('Hello NEW World');
    expect(test.hello_locale).toBeDefined();
    expect(test.hello_locale.get('en')).toMatch('Hello NEW World');
    expect(test.hello_locale.get('de')).toMatch('Hallo Welt');
  });

  it('should update dictionary in document', async () => {
    const schema = new Schema({ hello: { type: String, locale: true } });
    schema.plugin(locale, { languages: ['en', 'de'] });
    const Test = model('Test5', schema);
    const test = new Test({
      hello: {
        en: 'Hello World',
        de: 'Hallo Welt',
      },
    });
    await test.save();
    test.set('hello', { de: 'Hallo Neue Welt' });
    expect(test.hello).toMatch('Hello World');
    expect(test.setLocale('de').hello).toMatch('Hallo Neue Welt');
    expect(test.hello_locale).toBeDefined();
    expect(test.hello_locale.get('de')).toMatch('Hallo Neue Welt');
    expect(test.hello_locale.get('en')).toMatch('Hello World');
  });

  afterAll(async () => {
    connection.close();
  });
});

describe('Error Handling', () => {
  let connection;
  beforeAll(async () => {
    connection = await connect;
  });

  it.todo();

  afterAll(async () => {
    connection.close();
  });
});
